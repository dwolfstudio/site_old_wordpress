<?php
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

/**
 * Implement the Custom Header feature.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/customizer.php';

/* Cateory Thumbnails Plugin */
add_theme_support('category-thumbnails');
add_theme_support( 'post-thumbnails', array( 'post', 'movie', 'page' ) ); 

/* Multiple images in Post */
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Header',
            'id' => 'header',
            'post_type' => 'post'
        )
    );
}
/*Pagination*/
function pagination($pages = '', $range = 1)
{  
     $showitems = ($range * 1);  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"row text-center\"><ul class=\"pagination text-center \">";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."' class=\"arrows\"><img src=\"".esc_url( get_template_directory_uri() )."/images/arrow-first.png\"></a></li>";
         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."' class=\"arrows\"><img src=\"".esc_url( get_template_directory_uri() )."/images/arrow-prev.png\"></a></li>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li><span class=\"current custom-pag\"><p>".$i."</p></span></li>":"<li><a href='".get_pagenum_link($i)."' class=\"inactive custom-pag\"><p>".$i."</p></a></li>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\" class=\"arrows\"><img src=\"".esc_url( get_template_directory_uri() )."/images/arrow-next.png\"></a></li>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."' class=\"arrows\"> <img src=\"".esc_url( get_template_directory_uri() )."/images/arrow-last.png\"> </a></li>";
         echo "</ul></div></div>\n";
     }
}
register_nav_menu( 'primary', __( 'Primary Menu', 'yourthemename' ) );

/* LOAD CSS && JQUERY FROM CONTACT FORM ONLY ON CONTACT PAGE */
add_action( 'wp_print_scripts', 'my_deregister_javascript', 100 );
function my_deregister_javascript() {
if ( !is_page('Contact') ) {
wp_deregister_script( 'contact-form-7' );
}
}

add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
function my_deregister_styles() {
if ( !is_page('Contact') ) {
wp_deregister_style( 'contact-form-7' );
}
}
//footer menu
function register_my_menus() {
register_nav_menus(
array(
'new-menu' => __( 'Left Footer Menu' ),
'another-menu' => __( 'Right Footer Menu' ),
)
);
}
add_action( 'init', 'register_my_menus' );
//contact