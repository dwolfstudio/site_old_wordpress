<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header(); ?>
   <?php 
		$page_id = get_the_id(); 
	?>  
	<?php if( array_key_exists($page_id, $pages_x_category) ){ ?>
		<?php
			// Se a pagina for relacionada com alguma categoria carrega a categoria com o content certo
			// do contrario carrega o template de pagina
			$category_id = $pages_x_category[$page_id];
			$category = get_category($category_id);
			$header_image = get_the_category_data($category_id)->url;

			if(!empty($header_image)){
				require_once('inc/header_image.php');
			}
		?>
		
		<?php 
			// Set category id to query
			$args = array('cat' => $category->cat_ID);
			// Paginate
			preg_match('/\/page\/?([0-9]{1,})?/', home_url( $wp->request ), $pages);
			$paged = (isset($pages[1]) && !empty($pages[1]) ) ? $pages[1] : 1;
			$args['paged'] = $paged;
			


			switch ($category->cat_ID) {
				// Responsabilidades
				case 9:
					// Posts per page
					$args['posts_per_page'] = get_option('posts_per_page');
					break;
				default:
					// Posts per page
					$args['posts_per_page'] = get_option('posts_per_page');
					// Body size
					$content_type = 'large' ;
					// Template
					$template_type = 'grid';
					break;
			}

			query_posts( $args );
		?>
		<?php if ($category->cat_ID == 6) {?>
			<?php get_template_part( 'inc/carousel-marcas' ); ?>
		<?php } ?>
			<div class="container content-<?php echo $content_type ?>">
				<?php if (! in_array($category->cat_ID , [6, 8])) { ?>
					<h1 class="style-recipe"><?php echo $category->name ?></h1>
				<?php } ?>
				<?php if ( have_posts() ) { ?>
					<?php
						require_once('content-'.$template_type.'.php');
					?>
				<?php }else { ?>
					<!-- <div class="entry-content" style="padding-bottom: 30px;"><?php echo category_description( $category_id ); ?></div> -->
						<?php require_once('content-'.$template_type.'.php');  ?>
				<?php } ?>
			</div>
			
	<?php }else{ ?>
		<div class="container content-medium">
			
			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				// Include the page content template.
				get_template_part( 'content', 'page' );

			// End the loop.
			endwhile;
			?>
		</div>
	<?php } ?>
<?php get_footer(); ?>