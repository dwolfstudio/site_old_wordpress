<?php 
/* 
Template Name: Foto
*/
?>	
<?php get_header(); ?>
	<?php
		$title = get_the_title();
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	?>
<section
	id="intro" 
	class="header-background"
	style="background-image:url('<?php echo $image[0] ?>');" 
	data-speed="2" 
	data-type="background"
>
</section>

	<div class="container content-large receitas">
		<div class="col-xs-12">
				<header>
					<h1 class="entry-title">VÍDEOS</h1>	
				</header>
		</div>
	</div>
<div class="container content-large receitas">
<div class="content_receitas">
					<?php query_posts( array('cat' => 10, 'posts_per_page' => -1) ); ?> 

		<?php while (have_posts()) : the_post();
			$title_post = get_the_title();
			$image_post = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
			$link_vid = get_post_meta($post->ID, 'link_video');
			$content_post = get_the_content('');
		?>

		<?php /* https://www.youtube.com/watch?v=<?php echo $link_vid[0] */ ?>
			<div class="youtube-lightbox">
				<a href="#" id="<?php echo $post->ID; ?>" class="youtube-btn" >
					<div class="imagem_video">
						<div class="col-xs-12 col-md-5">
							<img src="http://i2.ytimg.com/vi/<?php echo $link_vid[0] ?>/mqdefault.jpg">
						</div>
						<div class="col-xs-12 col-md-7">
							<h2><?php echo $title_post ?></h2>
							<p><?php echo $content_post ?></p>
						</div>
					</div>
				</a>
				<div id="modal-youtube-<?php echo $post->ID; ?>" class="embed-youtube">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $link_vid[0] ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>	
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
		<div class="content_receitas">
			<div class="col-xs-12">
				<header>
					<h1 class="entry-title">GALERIA DE FOTOS</h1>	
				</header>
		
		<?php query_posts( array('cat' => 11, 'posts_per_page' => -1) ); ?> 
			<?php while (have_posts()) : the_post();
				$title_post = get_the_title();
				$image_post = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				$content_post = get_the_content('');
			?>
				<div class="col-xs-12 col-md-4">
					<a href="<?php echo get_permalink(); ?>">
						<div class="imagem_foto" style="background-image: url(<?php echo $image_post[0] ?>); width: 100%; height: 300px; margin-bottom: 50px;">
							<p class="texto_foto"><?php echo $title_post ?></p>
						</div>
					</a>
				</div>
				<?php endwhile; ?>
			</div>
		</div>
</div>
<?php get_footer(); ?>
