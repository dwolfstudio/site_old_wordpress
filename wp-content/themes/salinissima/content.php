<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<?php 
	// Get image header if exists in post else take the category's header 
	if (class_exists('MultiPostThumbnails')){
		$header_image = MultiPostThumbnails::get_post_thumbnail_id(
			get_post_type(),
			'header',
			$post->ID
		);

		if(isset($header_image[0]) && !empty($header_image[0])){
			$header_image = wp_get_attachment_image_src($header_image, 'full');
			$header_image = $header_image[0];
		}else{
			$category_id = wp_get_post_categories($post->ID);
			$category_id = $category_id[0];
			$header_image = get_the_category_data($category_id);
			$header_image = $header_image->url;
		}

	}

	if( isset($header_image) && !empty($header_image) ){
		require_once('inc/header_image.php');
	}
?>

<?php 
global $post;
	$categoria = get_the_category($post->id);
	$nome_cat = $categoria[0]->slug;
	$catId = $categoria[0]->cat_ID;

	switch ($catId) {
		case 7:
			require_once ("content-" . $nome_cat . ".php");
			break;
		case 3:
			require_once ("content-" . $nome_cat . ".php");
			break;
		case 9:
			require_once ("content-" . $nome_cat . ".php");
			break;
		case 11:
			require_once ("content-" . $nome_cat . ".php");
			break;
		default:
			require_once ("content-default.php");
			break;
	}
?>