function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

$(document).ready(function () {
  /* START MENU */
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    overlay.click(function () {
      $('.hamburger').trigger( "click" );
    });

    function hamburger_cross() {

      if (isClosed == true) {          

        trigger.removeClass('is-open');
        trigger.addClass('is-closed');      
        overlay.removeClass('over-open');
        isClosed = false;

      } else {   

        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        overlay.addClass('over-open');
        isClosed = true;

      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });
  /* END MENU */

  /* START AGE VERIFY */
  $('#age-yes').click(function(){
    var cookie_age = 1;
    if( $('#checkbox-age').is(':checked') ){
      cookie_age = 365;
    }

    setCookie('age', 'true', cookie_age);

    $('#container-age').fadeOut(function(){
      $('#overlay-age').fadeOut();
    });
  });
   $( "#click-terms" ).click(function() {
    $( "#use-terms" ).fadeIn( "slow", function() {
      $( ".verify-age" ).fadeOut( "slow", function() {
      });
    });
  });
  $( "#click-back" ).click(function() {
    $( "#use-terms" ).fadeOut( "slow", function() {
      $( ".verify-age" ).fadeIn( "slow", function() {
      });
    });
}); 
  /* END AGE VERIFY */
  /* START YOUTUBE */
  $('.youtube-btn').click(function(e){
    e.preventDefault();
    var youtube = $(this).attr('id');
    var lightbox = $('#modal-youtube-'+youtube);
    console.log(lightbox);
    $.fancybox({
      maxWidth     : '560px',
      maxHeight : '315px',
      openEffect  : 'elastic',
      closeEffect : 'elastic',
      height    : 'auto',
      fitToView: false,
      autoSize : false,
      wrapCSS : 'fancybox-texture',
      'content' : lightbox.html(),
      helpers: {
        overlay: {
          locked: false
        }
      }
      });
  })
/* END YOUTUBE */   
/* START GALERIA */   
   $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    centerMode: true,
    focusOnSelect: true
  });
/* END GALERIA */   

  /* START FOTO */
      $(".fancybox-foto").fancybox({
          title         : this.title,
          padding   : 0,
          maxWidth  : '100%',
          maxHeight : '100%',
          width   : 960,
          height    : 720,
          autoSize  : true,
          closeClick  : true,
          openEffect  : 'elastic',
          closeEffect : 'elastic',
          helpers: {
            overlay: {
              locked: false
            }
          }
      });
  /* END FOTO */
  /* START BUY */
  $('.buy-online').click(function(e){
    e.preventDefault();
    var produto = $(this).parent().parent().attr('id');
    var lightbox = $('#lightbox-'+produto);
     console.log(produto);
    $.fancybox({
      width     : '90%',
      maxWidth     : 300,
      openEffect  : 'elastic',
      closeEffect : 'elastic',
      height    : 'auto',
      fitToView: false,
      autoSize : false,
      wrapCSS : 'fancybox-texture',
      'content' : lightbox.html(),
      helpers: {
        overlay: {
          locked: false
        }
      }
      });
  })
  /* END BUY */
  // START MARCAS
    $(".content-marcas a").click(function(){
        $(".posts-list-item").hide();
        $(".content-marcas a img").addClass("filter-gray");
        $(this).children().removeClass("filter-gray");
        $(this).css("opacity", "1");
        var classe = '.' + $(this).attr("id");
        $(classe).fadeIn(400);
    });
        var url = location.hash;  
        $(url).trigger("click");
});
