<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<?php
	// Start the loop.
	
	while ( have_posts() ) : the_post();
			get_template_part( 'content', get_post_format() );
	// End the loop.
	endwhile;
	?>
<?php get_footer(); ?>
