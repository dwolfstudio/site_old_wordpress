<?php
	$title = get_the_title();
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	$img_destaque = get_post_meta($post->ID, 'imagem_destaque');
	$ingrediente = get_post_meta($post->ID, 'ingrediente');
	$preparo = get_post_meta($post->ID, 'modo_preparo');
	$destaque_post = get_post_meta($post->ID, 'destaque_post');
	$content = get_the_content('');
?>
<section
	id="intro" 
	style="background-image:url('<?php echo $img_destaque[0] ?>');" 
	data-speed="2" 
	data-type="background"
>
</section>
	
		
<div class="container content-large">
	<div class="row">
		<div class="col-xs-12">
			<header>
				<h1 class="entry-title"><?php echo $title ?></h1>	
			</header>
				<?php if ($destaque_post==true) { ?>
				<div class="texto_receitas space_top_40 col-xs-8">
				 	<?php echo $content ?>
				 </div>
				<div class="imagem_receitas col-xs-4 space_top_40 space_bot_150">
					<img src="<?php echo $destaque_post[0]; ?>">
				</div>
				<?php } else { ?>
				<div class="texto_receitas space_top_40 space_bot_150 col-xs-12">
				 	<?php echo $content ?>
				 </div>
				<?php } ?>
		</div>
	</div>
</div>