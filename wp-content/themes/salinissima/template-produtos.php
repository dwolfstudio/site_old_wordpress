<?php 
/* 
Template Name: Produtos
*/
?>
<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
<?php
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		$title = get_the_title();
	?>
<section 
	id="intro" 
	style="background-image:url('<?php echo $image[0] ?>'); background-position: 0px 10%;" 
	data-speed="2" 
	data-type="background"
>
</section>
<?php endwhile; ?>
	<div class="bg-content">
		<div class="container content-large">
			<?php query_posts( array('cat' => 6, 'posts_per_page' => -1) ); ?> 
			<?php while (have_posts()) : the_post(); ?>
			<?php
				$title = get_the_title();
				$content = get_the_content('');
				$image_produto = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
			?>
				<div class="company-align col-xs-12 col-md-10 col-md-offset-1 produto_liquid">

		        	    <header class="entry-header">
							<div class="produto">
								<div class="img-produto">
									<img src="<?php echo $image_produto[0] ?>">
								</div>
								<div class="conteudo-produto">
									<h4>COMPRAR</h4>
									<p style="margin-left: 3px;"><?php echo $content ?></p>
									<a href="#" class="buy-online btn-see-more">buscar lojas</a>
								</div>
							</div>
						</header>
				</div>
				<div id="lightbox-produto-<?php echo $post->ID ?>" class="buy-online-lightbox-container">
				<div class="buy-online-lightbox">
					<div class="row">
						<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h2 class="style-recipe text-center" style="margin-bottom:20px;">Lojas</h2>
						</div>
					</div>
					<?php foreach ($compra_online as $compra) { ?>
						<?php 
							$compra = explode(',' , $compra);
							$compra[1] = trim($compra[1]);
							$compra[1] = (strpos($compra[1], 'http://') === false && strpos($compra[1], 'https://') === false) ? 'http://'.$compra[1] : $compra[1];
							$clickRef =  "onde_comprar_" . str_replace(' ', '_', $compra[0]) . "_" . str_replace( ' ', "_", get_the_title());
						?>
						<div class="row">
							<div class="store-link col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<a href="<?php echo $compra[1] ?>" target="_blank" class="btn btn-default" onClick="ga('send', 'event', 'link', 'click', '<?php echo $clickRef;?>');">
									<span class="glyphicon glyphicon-shopping-cart"></span>
									<?php echo $compra[0] ?>
								</a>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
			<?php endwhile; ?>
								<div class="ornamento">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ornamento.png">
					</div>
		</div>
	</div>
<?php get_footer(); ?>