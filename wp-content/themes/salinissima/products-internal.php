<div class="container content-medium">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1><?php the_title() ?></h1>
		<div class="entry-content">
			<?php the_content() ?>
			<script>
  				jQuery('p iframe').wrap("<div class='iframe-flexible-container'></div>");		
			</script>
		</div>
	</article><!-- #post-## -->
</div>
