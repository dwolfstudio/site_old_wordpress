<?php
	$title = get_the_title();
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	$img_destaque = get_post_meta($post->ID, 'imagem_destaque');
	$ingrediente = get_post_meta($post->ID, 'ingrediente');
	$preparo = get_post_meta($post->ID, 'modo_preparo');
	$image_header = wp_get_attachment_image_src( get_post_thumbnail_id( 43 ), 'full' );
?>
<section
	id="intro" 
	style="background-image:url('<?php echo $image_header[0] ?>');" 
	data-speed="2" 
	data-type="background"
>
</section>

<div class="container content-large">
	<div class="row">
		<div class="col-xs-12">
			<header>
				<h1 class="entry-title"><?php echo $title ?></h1>	
			</header>
				<div class="texto_receitas space_top_40 col-xs-8">
				<img src="/wp-content/uploads/2016/01/ingredientes-thumb.png">
					<h4>Ingredientes:</h4>
					<p><?php echo $ingrediente[0] ?></p>
					<p><?php echo $ingrediente[1] ?></p>
					<p><?php echo $ingrediente[2] ?></p>
					<p><?php echo $ingrediente[3] ?></p>
					<p><?php echo $ingrediente[4] ?></p>
					<p><?php echo $ingrediente[5] ?></p>
					<p><?php echo $ingrediente[6] ?></p>
					<p><?php echo $ingrediente[7] ?></p>
					<p><?php echo $ingrediente[8] ?></p>
					<p><?php echo $ingrediente[9] ?></p>
					<p><?php echo $ingrediente[10] ?></p>
				<img class="space_top_40" src="/wp-content/uploads/2016/01/passos-thumb.png">
					<h4>Modo de Preparo:</h4>
					<p class="space_bot_150"><?php echo $preparo[0] ?></p>
				</div>
			<div class="imagem_receitas col-xs-4 space_top_40">
				<img src="<?php echo $image[0] ?>">
			</div>
		</div>
	</div>	
</div>