<?php
/**
 * The template for displaying products category
 */

get_header(); ?>
	<?php 
		$category = get_the_category();
		$header_image = get_the_category_data()->url;
		require_once('inc/header_image.php');
	?>
	
	<?php 
		// Set category id to query
		$args = array('cat' => $category[0]->cat_ID);
		// Paginate
		preg_match('/\/page\/?([0-9]{1,})?/', home_url( $wp->request ), $pages);
		$paged = (isset($pages[1]) && !empty($pages[1]) ) ? $pages[1] : 1;
		$args['paged'] = $paged;
		// Posts per page
		$args['posts_per_page'] = get_option('posts_per_page');

		if($category[0]->cat_ID == 5){
			// Body size
			$content_type = 'medium';
			// Template
			$template_type = 'list';
		}else{
			// Posts per page
			// Body size
			$content_type = 'large' ;
			// Template
			$template_type = 'grid';
		}

		query_posts( $args );
	?>
	
	<div class="container content-<?php echo $content_type ?>">
		<h1 class="style-recipe"><?php echo $category[0]->name ?></h1>
		
		<?php if ( have_posts() ) : ?>
			<?php
				require_once('content-'.$template_type.'.php');
			?>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>