<?php 
/* 
Template Name: Contato
*/
?>
<?php get_header(); ?>

<?php 
	$title = get_the_title(); 
?>

	<div class="container content-large">
			<div class="row">
				<div class="col-xs-12 ">
					<div class="titulo_contato col-xs-offset-2">
						<p><?php echo $title ?></p>
					</div>
					
					 <?php if (have_posts()) : while (have_posts()) : the_post();?>
					<div class="col-xs-12 texto_contato">
						<?php the_content(); ?>
					</div>
					<?php endwhile; endif; ?>

				</div>
					<div class="ornamento">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ornamento.png">
		</div>
			</div>
	</div>

<?php get_footer(); ?>