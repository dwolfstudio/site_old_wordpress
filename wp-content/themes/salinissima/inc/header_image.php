<style>
    #intro { 
        background: url("") 50% 0 fixed; 
        height: auto;  
        margin: 0 auto; 
        width: 100%; 
        position: relative; 
        background-repeat: no-repeat; 
    }
    /* Large desktops and laptops */
    @media (min-width: 1200px) {
        #intro {
            padding: 15% 0;
            background-size: 100%;
        }
    }

    /* Portrait tablets and medium desktops */
    @media (min-width: 992px) and (max-width: 1199px) {
        #intro {
            padding: 15% 0;
            background-size: 100%;
        }
    }

    /* Portrait tablets and small desktops */
    @media (min-width: 768px) and (max-width: 991px) {
        #intro {
            padding: 17% 0;
            background-size: 100%;
        }
    }

    /* Landscape phones and portrait tablets */
    @media (max-width: 767px) {
        #intro {
            padding: 20% 0;
            background-size: 140%;
        }
    }   

    /* Landscape phones and smaller */
    @media (max-width: 480px) {
        #intro {
            padding: 20% 0;
            background-size: 150%;
        }
    }
</style>
<section id="intro" style="background-image:url('<?php echo $header_image ?>');" data-speed="2" data-type="background">

</section>

<script>
    $(document).ready(function(){
       // cache the window object
       $window = $(window);
        
         // declare the variable to affect the defined data-type
         var $scroll = $('#intro');
                         
          $(window).scroll(function() {
            // HTML5 proves useful for helping with creating JS functions!
            // also, negative value because we're scrolling upwards                            
            var yPos = -($window.scrollTop() / $scroll.data('speed'));
             
            // background position
            var coords = '50% '+ yPos + 'px';
     
            // move the background
            $scroll.css({ backgroundPosition: coords });   
          }); // end window scroll

    }); // close out script

    /* Create HTML5 element for IE */
    document.createElement("section");
</script>