<div class="container content-medium">
	<h1 class="style-recipe">Nossas Marcas</h1>
	<p>O portfolio de produtos da Natique combina o melhor dos pequenos produtores do Brasil com os rótulos que já são sucesso no mundo, fruto da associação com a espanhola Osborne. Valorizamos os processos de produção artesanais e ecológicos, estimulando a economia de cada região e trazendo ao público tradição, inovação e qualidade. </p>
	<b>Confira nossos produtos abaixo ou<a href="<?php get_site_url(); ?>/wp-content/uploads/2015/12/portfolio.pdf" target="_blank" onClick="ga('send', 'event', 'link', 'click', 'pdf_portifolio'); style="font-weight: bold; font-size: 14px; color: #6F6F6F;"> clique aqui</a> para fazer o download do portfólio completo.</b> 
	
	
</div>
<div class="row content-marcas">
	<a id="santo-grau"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_sg_reflexo_cor.png" alt="" class="img-responsive filter-gray"></a>
	<a id="espirito-minas"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_edm_reflexo_cor.png" alt="" class="img-responsive filter-gray"></a>
	<a id="osborne"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo brandy_reflexo_cor.png" alt="" class="img-responsive filter-gray"></a>
	<a id="xiboquinha"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_xiboq_reflexo_cor.png" alt="" class="img-responsive filter-gray"></a>
	<a id="liquid"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_liquid_reflexo.png" alt="" class="img-responsive filter-gray"></a>
	<a id="becosa"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/becosa-home.png" alt="" class="img-responsive filter-gray"></a>
</div>
<script type="text/javascript">
	$(document).ready(function(){
	  $('.content-marcas').slick({
	  	 	slidesToShow: 5,
			slidesToScroll: 1,
			autoplay: true,
			arrows: true,
			infinite: false,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 4,
						arrows: false	
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 3,
						arrows: false
					}
				},
				{	
					breakpoint: 480,
					settings: {
						slidesToShow: 2,
						arrows: false
					}
				},	
			]
	  });
	});
</script>