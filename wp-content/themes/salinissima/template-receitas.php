<?php 
/* 
Template Name: SuperReceitas
*/
?>
<?php get_header(); ?>
<?php
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		$title = get_the_title();
		$content = get_post('');
	?>
<section
	id="intro" 
	style="background-image:url('<?php echo $image[0] ?>'); background-position: 0px 10%;" 
	data-speed="2" 
	data-type="background"
>
</section>

<div class="bg-content">
	<div class="container content-large receitas">
			<div class="col-xs-12">
				<div class="h1_receitas"><h1><?php echo $title ?></h1></div>
				<p class="p-receitas"><?php 
				echo($content->post_content) ?></p>
			</div>
	</div>
	<div class="content_receitas">
		<div class="container content-large">

 <?php 
 	preg_match('/\/page\/?([0-9]{1,})?/', home_url( $wp->request ), $pages);
	$paged = (isset($pages[1]) && !empty($pages[1]) ) ? $pages[1] : 1;
	query_posts( array('cat' => 7, 'posts_per_page' => 4, 'paged' => $paged) ); ?>  

	<?php while (have_posts()) : the_post();
		$title_post = get_the_title();
		$image_post = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		$content = get_the_content('');
		$intro_receita = get_post_meta($post->ID, 'intro_receita')[0];
		
	?>
		
			<div class="company-align col-xs-12">
				    <div class="img_comprar col-xs-12 col-md-3 col-md-offset-2" >
						<a href="<?php the_permalink() ?>">
							<img src="<?php echo $image_post[0] ?>" style="border:5px; border-color: #4f2677;">
						</a>
					</div>
					<div class=" texto_comprar col-md-6 col-xs-12 ">
		           		<a href="<?php the_permalink() ?>">
		            		<header class="entry-header">
								<h4 class="entry-title titulo_receitas text-left"><?php echo $title_post ?></h4>
								<span class="entry-excerpt"><?php echo $intro_receita ?></span>
								<a href="<?php the_permalink() ?>" class="btn-see-more text-center"> veja mais</a></span>
							</header>
						</a>
					</div>
			</div>
	<?php endwhile; ?>
		<?php if (function_exists("pagination")) {
			pagination($additional_loop->max_num_pages);
		} ?>
		</div>	
	</div>
</div>

<?php get_footer(); ?>