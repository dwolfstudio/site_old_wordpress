<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
 
get_header(); ?>
 
<!-- BXslider -->
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.bxslider.css" type="text/css" />
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.bxslider.js"></script>

<?php query_posts( array('meta_key'=>'prioridade',  'orderby' => 'meta_value_num', 'order' => 'ASC', 'cat' => 1, 'posts_per_page' => -1) ); ?>
<?php if ( have_posts() ) : ?>
	<div id="carousel-home">
	
	<ul class="bxslider">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php
				$title = get_the_title();
				$content = get_the_content('');
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

			?>
			<?php 

				$carousel_title = get_post_meta($post->ID, 'cor-titulo')[0];
				$carousel_texto = get_post_meta($post->ID, 'cor-texto')[0];
				$bot_chamada = get_post_meta($post->ID, 'bot_chamada')[0];
				$cor_seta = get_post_meta($post->ID, 'cor_seta')[0];

			?>
			<?php if(isset($image[0]) && !empty($image[0])){ ?>
				<?php 
					// Try to get video (if is video)
					$video_url = get_post_meta($post->ID, 'youtube'); 
					if(!empty($video_url)){
						preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $video_url[0], $video_url);
						$video_url = $video_url[0];
					}
					// Try to get link (if is has)
					$link = get_post_meta($post->ID, 'link');
					
				?>

				<?php if(!empty($video_url)){ ?>
					<li>
						<a class="youtube fancybox fancybox.iframe" title="<?php echo $title ?>" href="//youtube.com/embed/<?php echo $video_url ?>">
							<img src="<?php echo $image[0]; ?>">
							<?php if(!empty($title)){ ?>
								<div class="fancybox-inner bx-caption-container">
									<h4 style="color: <?php echo $carousel_title; ?> "><?php echo $title ?></h4>
									<p style="color: <?php echo $carousel_texto; ?> "><?php echo $content ?></p>
									<a class="youtube fancybox fancybox.iframe btn btn-default" title="<?php echo $title ?>" href="//youtube.com/embed/<?php echo $video_url ?>"><?php echo __("<!--:en-->Watch video<!--:--><!--:br-->Assistir video<!--:--><!--:es-->Ver video<!--:-->"); ?> <span class="glyphicon glyphicon-chevron-right"></span></a>
								</div>
							<?php } ?>
						</a>
					</li>
				<?php }elseif(!empty($link)){ ?>
					<?php 
						$link_domain = parse_url($link[0], PHP_URL_HOST);
						$link_target = ($_SERVER['HTTP_HOST'] == $link_domain) ? '' : '_blank';
					?>
					<li>
						<a href="<?php echo $link[0] ?>" target="">
							<img src="<?php echo $image[0]; ?>">
							<?php if(!empty($title)){ ?>
								<div class="bx-caption-container">
									<div class="texto_home">
										<h4 style="color: <?php echo $carousel_title; ?> !important"><?php echo $title ?></h4>
										<p style="color: <?php echo $carousel_texto; ?> !important"><?php echo $content ?></p>
										<a href="<?php echo $link[0] ?>" target="" class="btn btn-default">
										<?php echo $bot_chamada ?>
										 <i class="fa fa-chevron-right" style="color: #fff " aria-hidden="true"></i></a>
									</div> 
								</div>
							<?php } ?>
						</a>
					</li>
				<?php }else{ ?>
					<li>
						<img src="<?php echo $image[0]; ?>">
						<?php if(!empty($title)){ ?>
							<div class="bx-caption-container">
								<h4 style="color: <?php echo $carousel_title; ?> !important "><?php echo $title ?></h4>
								<p style="color: <?php echo $carousel_texto; ?> !important "><?php echo $content ?></p>
							</div>
						<?php } ?>
					</li>
				<?php } ?>
			<?php } ?>
		<?php endwhile; ?>
	</ul>
	</div>
<?php endif; ?>

<script>
	jQuery('.bxslider').bxSlider({
		adaptiveHeight: true,
		captions: true,
		autoStart: true,
		auto: true,
		pause: 8000,
		speed: 1000, 
	});
</script>
<div class="destaques">
	<div class="container content-large mobile-destaque">
		<div class="">
			<div class="destaque_home col-ms-12 col-md-12 col-lg-12 ">
				<h2>DESTAQUES</h2>
			</div>
	<ul>
	<?php wp_reset_query(); ?>
	<?php query_posts( array('cat' => 2, 'posts_per_page' => -1) ); ?>
	<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<?php
				$title = get_the_title();
				$content = get_the_content('');
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				$bot_chamada = get_post_meta($post->ID, 'bot_chamada');
				$link = get_post_meta($post->ID, 'link');
				$link_post = get_permalink();
			?>
			<div class="col-xs-12 col-md-4 col-lg-4 mob-destaque">
			<article id="post-<?php echo $post->ID ?>" class="posts-grid-item brandy-effect ">
				<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full'); ?> 
				<div class="col-xs-12" style="background-color: transparent; width:100%; margin-bottom: 10px;">
					<img class="link-conheca" src="<?php echo $image[0] ?>">	
				</div>
				<div class="conteudo-bot-home" style="margin-top: 10px;">
					<h4><?php echo $title ?></h4>
					<p><?php echo $content ?></p>
				<a href="<?php echo $link[0] ?>">
					<div class="bot_chamada text-center" style="height:21px;">
						 <?php echo $bot_chamada[0]; ?>
					</div>
				</a>
				</div>
			</article>
		</div>

		
		<?php endwhile; ?>
	<?php endif; ?>
	</ul>
			<div class="ornamento">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ornamento.png">
		</div>
	</div>
	</div>
</div>
<?php get_footer(); ?>
