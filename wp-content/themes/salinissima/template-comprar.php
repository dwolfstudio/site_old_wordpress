<?php 
/* 
Template Name: Comprar
*/
?>	
<?php get_header(); ?>

	<?php
		$title = get_the_title();
		$content = get_the_content('');
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	?>

<section
	id="intro" 
	class="header-background"
	style="background-image:url('<?php echo $image[0] ?>');" 
	data-speed="2" 
	data-type="background"
>
</section>
<div class="bg-content">
	<div class="container content-large" style="padding-top:20px !important;">
		<div class="col-xs-12 .space_bot_5p">
			<h4 class="entry-title titulo_comprar">COMPRAR</h4>	
			<?php query_posts( array('cat' => 6, 'posts_per_page' => -1) ); ?> 
			<?php while (have_posts()) : the_post();
				$title_post = get_the_title();
				$image_post = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				$compra_online = get_post_meta($post->ID, 'compra_online');
			?>
    	    <div class="img_comprar col-xs-12 col-md-4 space_top_50" >
				<img src="<?php echo $image_post[0] ?>" style="border:5px; border-color: #4f2677;">
			</div>
			<div id="produto-<?php echo $post->ID ?>" class="out-thumb brandy-comprar texto_comprar col-xs-12 col-md-8 space_top_50 ">
        		<a   href="#" class="buy-online">
            		<header class="entry-header " >
						<h4 class="entry-title titulo_comprar text-capitalize"><?php echo $title_post; ?></h4>
						<span class="entry-excerpt"><?php the_content(false); ?></span>
						<a href="#" class="buy-online btn-see-more"><i class="fa fa-shopping-cart" style="margin-top: 6px;"></i> Comprar</a></span>
					</header>
				</a>
			</div>
		</div>
				<div class="ornamento">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ornamento.png">
		</div>
	</div>
		<div id="lightbox-produto-<?php echo $post->ID ?>" class="buy-online-lightbox-container">
				<div class="buy-online-lightbox">
					<div class="row">
						<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h2 class="style-recipe text-center" style="margin-bottom:20px;">Lojas</h2>
						</div>
					</div>
					
					<?php foreach ($compra_online as $compra) { ?>

						<?php 
							$compra = explode(',' , $compra);
							$compra[1] = trim($compra[1]);
							$compra[1] = (strpos($compra[1], 'http://') === false && strpos($compra[1], 'https://') === false) ? 'http://'.$compra[1] : $compra[1];
							$clickRef =  "onde_comprar_" . str_replace(' ', '_', $compra[0]) . "_" . str_replace( ' ', "_", get_the_title());
						?>
						<div class="row">
							<div class="store-link col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<a href="<?php echo $compra[1] ?>" target="_blank" class="btn btn-default" onClick="ga('send', 'event', 'link', 'click', '<?php echo $clickRef;?>');">
									<i class="fa fa-shopping-cart" style="color: #f7eed7"></i>
									<?php echo $compra[0] ?>
								</a>
							</div>
						</div>
					<?php } ?>
				</div>
	<?php endwhile; ?> 

			</div>

</div>
<?php get_footer(); ?>