<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
  <title><?php bloginfo('name'); ?></title>
  
  <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/slick.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/slick-theme.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.fancybox.css" media="screen" />

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/main.css">


  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js"></script>

  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/slick.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/parallax.js"></script>

  <!-- FancyBox -->
  <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.fancybox.js"></script>

  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/main.js"></script>
  

  <link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/faviconn.png">

  <!-- FancyBox -->




	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> id="wrapper">
	<?php if( !isset($_COOKIE['age']) || empty($_COOKIE['age']) ){ ?>
   <div id="overlay-age" class="overlay-age">
    </div>
    <div id="container-age" class="verify-age ">
      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 fix-span img_age">
        <img src="/wp-content/uploads/2016/01/retangulo-roxo-18-anos.png" />
      </div>
      <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <div class=" text-acess">
          <p>O ACESSO PARA ESTE SITE, ASSIM COMO O CONSUMO DE BEBIDAS ALCOÓLICAS, SÓ É PERMITIDO PARA MAIORES DE IDADE.</p>
        </div>
        <div class=" text-18">
          <p>VOCÊ POSSUI MAIS DE 18 ANOS?</p>
        </div>
        <div class="button-age row">
          <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <button id="age-yes" class="yes pull-right">SIM</button>
          </div>
          <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <a href="http://google.com"><button id="age-no" class="no pull-left">NÃO</button></a>
          </div>
        </div>
        <div class=" remember-me">
          <form>
              <div class="checkbox">
                <label for="checkbox-age">
                  <input type="checkbox" id="checkbox-age" />Lembrar-me
                </label>
              </div>
            </form>
            <p>Clicando em SIM, você está aceitando os <a id="click-terms" href="#">termos de uso</a> do site.</p>
        </div> 
      </div>
    </div>
    <div id="use-terms">
        <div id="back-age">
          <button id="click-back" class="btn-back-age">VOLTAR</button>
        </div>   
        <h5>TERMOS E CONDIÇÕES DE USO</h5>
        <p>O presente Termos e Condições de Uso (“Termo”) visa regular a utilização pelo público em geral (doravante denominado “Usuário”) das páginas, aplicativos, redes sociais, perfis, dentre outros (doravante denominados isolada ou conjuntamente  como “conteúdo”), de responsabilidade da Natique Indústria e Comércio Ltda. (“Natique”), aplicando-se sobre, e englobando, todas as marcas de titularidade da Natique, produtos e respectivas informações. 
        São marcas da Natique todas aquelas elencadas em seu site oficial.
        O presente Termo está livremente disponível para acesso pelo Usuário por meio do endereço <a href="">http://www.santograu.com/br/termos-uso.</a>
        É reservado à Natique o direito de modificar este Termo a qualquer tempo, sem prévio aviso, e a seu exclusivo critério. É de responsabilidade do Usuário verificar o teor do Termo periodicamente para conhecer eventuais atualizações, que se tornarão efetivas no momento da publicação.</p>
        <h5>1. ACEITAÇÃO</h5>
        <p>Ao manter o acesso ao conteúdo, o Usuário automaticamente aceita integralmente o presente Termo.</p>
        <h5>2. PROIBIÇÃO DE ACESSO A MENORES DE IDADE</h5>
        <p>O conteúdo somente poderá ser acessado por Usuários maiores de 18 anos, não podendo ser a Natique responsabilizada por acessos indevidos.</p>
        <h5>3. UTILIZAÇÃO DO CONTEÚDO</h5>
        <p>Ao acessar e utilizar o conteúdo, o Usuário compromete-se a respeitar a legislação brasileira vigente, bem como ao quanto disposto neste Termo, obrigando-se a não produzir, disponibilizar, divulgar, compartilhar ou transmitir qualquer informação ou material (“material”) dentro do conteúdo ou de seus espaços interativos que: (i) esteja em desconformidade com as normas da legislação brasileira, bem como à moral e aos bons costumes geralmente aceitos; (ii) incentive, seja conivente com ou estimule o consumo de bebidas alcoólicas por menores, o consumo excessivo ou a condução de veículos sob a influência de álcool; (iii) estimule a discriminação, o ódio ou a violência contra pessoas ou grupos em razão de sexo, raça, religião, deficiência, nacionalidade, posição política/eleitoral ou por qualquer outro motivo; disponibilize ou permita acessar produtos, elementos, mensagens e/ou serviços ilegais, violentos, pornográficos, degradantes ou, em geral, contrários à lei, à moral, à ordem pública, e aos bons costumes geralmente aceitos; (iv) seja falso, ambíguo, inexato, exagerado, fora de contexto, de forma que possa induzir quem acessa a informação a erro; (v) esteja protegido por direitos de propriedade intelectual ou industrial de terceiros, sem que o Usuário tenha obtido autorização necessária para utilização por parte dos titulares dos direitos; (vi) coloquem em risco tecnológico, causem dano, prejudiquem o funcionamento da rede, dos sistemas ou equipamentos de terceiros, ou envolvam vírus, dentre outros.
        O Usuário será sempre o único responsável pelo uso que fizer do conteúdo ou dos espaços interativos, bem como por qualquer material por ele inserido. A Natique poderá ou não exercer controle editorial sobre o material inserido por qualquer Usuário. Assim sendo, um Usuário pode ser exposto a material inserido por outros Usuários que possa vir a ser incorreto, ilegal ou ofensivo, sem que isso implique responsabilidade à Natique.</p>
        <h5>4. RESPEITO AOS DIREITOS DE PROPRIEDADE INTELECTUAL</h5>
        <p>O Usuário compromete-se a utilizar qualquer informação disponibilizada no conteúdo em total consonância com o ordenamento jurídico, com a moral e os bons costumes geralmente aceitos e com o disposto neste Termo.
        Todas as marcas, os nomes comerciais, logotipos de qualquer espécie, fotos e imagens disponibilizados são de propriedade exclusiva da Natique ou têm seu uso autorizado, sendo que o acesso a tal conteúdo não pode ser entendido como autorização para que o Usuário possa citar, usar, copiar tais marcas, nomes comerciais, logotipos, fotos e imagens, ficando os infratores sujeitos às sanções civis e criminais correspondentes, nos termos da legislação em vigor.
        O Usuário deverá se abster de obter, ou de tentar obter, o conteúdo, total ou parcial, bem como de utilizá-lo, por meios distintos daqueles que, em cada caso, tenham sido colocados à disposição para tais propósitos.
        É vedado ao Usuário obter e utilizar dados com finalidade publicitária e/ou remeter publicidade de qualquer segmento com intuito comercial sem a prévia autorização, assim como enviar cadeias de mensagens eletrônicas não solicitadas nem previamente consentidas que possam de alguma forma comprometer a Natique.</p>
        <h5>5. RESTRIÇÃO, SUSPENSÃO E CANCELAMENTO</h5>
        <p>O acesso do Usuário ao conteúdo e espaços interativos poderá ser restringido, suspenso ou cancelado, sem aviso prévio, em caso de infração ao presente Termo, sem prejuízo de qualquer pedido de perdas e danos causados pelo Usuário. Ressaltamos que a Natique tem a capacidade de rastrear o endereço IP do Usuário e se necessário contatar o provedor de serviços de Internet.</p>
        <h5>6. BENS E SERVIÇOS DE TERCEIROS</h5>
        <p>A Natique não será responsável por quaisquer informações fornecidas, serviços prestados, ou produtos vendidos, por pessoas ou empresas que possam ser ofertados ou expostos no conteúdo ou serem acessados por meio dele, através de links.
        A Natique não será responsável por sites de terceiros, ainda que possam ser acessados por meio do conteúdo disponibilizado pela Natique (links).</p>
        <h5>7. FORO</h5>
        <p>Fica eleito o foro da Comarca de São Paulo, estado de São Paulo, para dirimir eventuais controvérsias decorrentes dos presentes Termos, com exclusão de qualquer outro, por mais privilegiado que seja.</p>
    </div>
  <?php } ?>

  <div class="overlay"></div>

  <!-- Fixed navbar -->
  <div class='menu'>
    <nav class="navbar navbar-inverse navbar-relative-top" style="position: fixed !important; top:0; z-index: 99999999; width: 100%;"> 
      <div class="container">
        <div class="menu-mobile navbar-header ">
          <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
              <span class="hamb-top"></span>
              <span class="hamb-middle"></span>
              <span class="hamb-bottom"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/menu_logo.png">
          </a>
          <?php if (function_exists('qts_language_menu') ) qts_language_menu('image'); ?>
        </div>

      </div>
    </nav>
    <!-- Sidebar -->
    
      <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
          <ul class="nav sidebar-nav">
              <li class="sidebar-brand">
                  MENU
              </li>
              <?php $menu = wp_get_nav_menu_items( 'principal' ); ?>
              <?php foreach ($menu as $item_menu) { ?>
                  <li>
                    <a href="<?php echo $item_menu->url ?>"><?php echo $item_menu->title ?></a>
                  </li>
              <?php } ?>
              
          </ul>
      </nav>
</div>
    
    <!-- /#sidebar-wrapper -->