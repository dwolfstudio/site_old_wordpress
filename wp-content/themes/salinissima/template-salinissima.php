<?php 
/* 
Template Name: Salinissima
*/
?>
<?php get_header(); ?>
<?php
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		$title = get_the_title();
	?>
<section
	id="intro" 
	style="background-image:url('<?php echo $image[0] ?>'); background-position: 0px 10%;" 
	data-speed="2" 
	data-type="background"
>
</section>
<div class="bg-content">
	<div class="container content-large manifesto">
			<div class="col-xs-12">
				<div class="h1_manifesto"><h1><?php echo $title ?></h1></div>
			</div>
	</div>
	<div class="content_manifesto">


	<?php 
 	preg_match('/\/page\/?([0-9]{1,})?/', home_url( $wp->request ), $pages);
	$paged = (isset($pages[1]) && !empty($pages[1]) ) ? $pages[1] : 1;
	query_posts( array('cat' => 10, 'posts_per_page' => -1, 'paged' => $paged) ); ?> 


<div class="container content-large content-manifest">

	<div class="company-align col-xs-12">
		<div class=" texto_manifesto col-md-12 col-xs-12 ">
			<?php while (have_posts()) : the_post();
				$title_post = get_the_title();
				$image_post = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				$content = get_the_content('');
			?>
    			<header class="entry-header">
					<h4 class="entry-title titulo_receitas text-left"><?php echo $title_post ?></h4>
					<span class="entry-excerpt" style="margin-left: 3px;"><?php echo $content ?></span>
			<?php endwhile; ?>
				</header>
	</div>
					<div class="ornamento">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ornamento.png">
					</div>
		</div>
</div>	
	</div>
</div>
</div>
</div>
<?php get_footer(); ?>