<?php
	$title = get_the_title();
	$content = get_the_content('');
	$gallery = get_post_gallery( get_the_ID(), false );
?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 estudio article-con" >
                <?php
                    if ( $gallery ) { ?>
					<div>
                        <h1> Galeria: </h1>
                    </div>
                        <div class="slider-for col-xs-12">
                           <?php foreach ( $gallery['src'] as $src ) { ?>
                                <div>
                                    <img  src="<?php echo $src ?>" alt="" style="width: 80%; height: 80%;"/>
                                </div>   
                            <?php } ?>
                        </div> 	

                        <div class="slider-nav col-xs-12">
                           <?php foreach ( $gallery['src'] as $src ) { ?>
                                <div>
                                    <img  src="<?php echo $src ?>" alt="" style="width: 60%; height: auto;"/>
                                </div>   
                            <?php } ?>
                        </div>
                <?php } ?>
            </div>
	</div>
</div>
