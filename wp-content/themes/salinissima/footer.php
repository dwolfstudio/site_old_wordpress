<?php wp_footer(); ?>
<div class="footer navbar">
	<div class="container">
		<div class="">
			<div class="col-md-12 text-center">
				<div class="icone_social">
					<p>
						<i class="fa fa-facebook"></i>
						<i class="fa fa-instagram"></i>
						<p><i class="fa fa-phone"></i>(11) 3875-1223</p>
						<p><i class="fa fa-envelope" aria-hidden="true"></i>contato@salinissima.com.br</p>
					</p>

				</div>
				
			</div>
		</div>
		<div class="">
			<div class="col-xs-12 col-md-12 align-content-link">
				<div class="align-link-left pull-left col-xs-6">
					<div class="pull-right">	
						<?php wp_nav_menu( array( 'theme_location' => 'new-menu' ) ); ?>
					</div>
				</div>
				<div class="align-link-left pull-left col-xs-6 text-left">
					<div class="pull-left">
						<?php wp_nav_menu( array( 'theme_location' => 'another-menu' ) ); ?>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>
<script type="text/javascript">
 (function(){
	var url = location.hash;
	$('body').scrollTo(url);
	});
</script>
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-65229970-6', 'auto');
 ga('send', 'pageview');

</script>
<!-- Facebook Conversion Code for Outras conversões no site - Santo Grau 1 -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6028088198366', {'value':'0.00','currency':'BRL'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6028088198366&amp;cd[value]=0.00&amp;cd[currency]=BRL&amp;noscript=1" /></noscript>
</body>
</html>
